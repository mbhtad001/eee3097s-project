The following instructiins detail how to use the program:

//To run the program and encrypt  file use the the command:
python3 Encryptor.py -enc "fileName"

//To run the program and decrypt  file use the the command:
python3 Encryptor.py -dec "fileName"

This program is also used as a module in the main program ICM20948.py
