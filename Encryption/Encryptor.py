import filecmp
from cryptography.fernet import Fernet
import time
import sys
import getopt






class encryptorClass:


    def encrypt(self, fileName):
        #creating encryption key
        key = Fernet.generate_key()

        with open('mykey.key', 'wb') as mykey:
            mykey.write(key)

        #Load in key
        with open('mykey.key', 'rb') as mykey:
            key = mykey.read()

        #pasing in key to fernet onject and loading in sensor data file
        f = Fernet(key)

        with open(fileName, 'rb') as original_file:
            original = original_file.read()

        #encrypt data with Ferent object that has the key
        encrypted = f.encrypt(original)

        #Finally, write encrypted data into new csv file called 'enc_sensor_data'
        with open('enc_sensor_data.csv', 'wb') as encrypted_file:
            encrypted_file.write(encrypted)

    def decrypt(self, fileName):

        with open('mykey.key', 'rb') as mykey:
            key = mykey.read()

        f = Fernet(key)

        with open(fileName, 'rb') as encrypted_file:
            encrypted = encrypted_file.read()

        decrypted = f.decrypt(encrypted)

        with open('dec_sensor_data.csv', 'wb') as decrypted_file:
            decrypted_file.write(decrypted)

    
    
    pass

if len(sys.argv) == 3:

    encryptor = encryptorClass()
    #argv = sys.argv[1:]

    if sys.argv[1] == '-dec':

        #opts, args = getopt.getopt(argv, 'dec:', ['foperand'])

        #for opt, arg in opts:
            #encryptor.decrypt(arg)

        print("\nDecrypting file....")
        time.sleep(0.1)
        start = time.time()
        encryptor.decrypt(sys.argv[2])
        finish = time.time() - start
        print("\nDone")
        print("\nTime taken to decrypt: {}s".format(finish))


    elif sys.argv[1] == "-enc":

        #opts, args = getopt.getopt(argv, 'enc:', ['foperand'])

        #for opt, arg in opts:
            #encryptor.encrypt(arg)

        print("\nEncrypting file....")
        time.sleep(0.1)
        start = time.time()
        encryptor.encrypt(sys.argv[2])
        finish = time.time() - start
        print("\nDone")

        print("\nTime taken to encrypt: {}s".format(finish))

    else:
        print ('usage: Encryptor.py -enc/-dec <first_operand>')
        sys.exit(2)
